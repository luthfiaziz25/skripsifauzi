import 'package:flutter/material.dart';
import 'package:rose_monitoring/ui/sample_1.dart';
import 'package:rose_monitoring/ui/sample_2.dart';

class LineChartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.teal,
      child: ListView(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.only(
                left: 36.0,
                top: 24,
              ),
              child: Text(
                'Rose Monitoring',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 32,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 28,
              right: 28,
            ),
            child: LineChartSample7(),
          ),
        ],
      ),
    );
  }
}