import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:rose_monitoring/ui/v_linechart.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title,this.itemBuilder,this.function}) : super(key: key);
  final String title;
  final function;
  final FirebaseAnimatedListItemBuilder itemBuilder;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DatabaseReference itemRef;
  Item item;
  List<Item> items = List();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var statusHujan;
  var nilaiHujan = 500;

  void handleSubmit() {
    final FormState form = formKey.currentState;
    if(form.validate()){
      form.save();
      form.reset();
      itemRef.push().set(item.toJson());
    }
  }

  _onEntryAdded(Event event) {
    setState(() {
      items.add(Item.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event) {
    var old = items.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      items[items.indexOf(old)] = Item.fromSnapshot(event.snapshot);
    });
  }
  var height = Size.fromHeight(50.0);
  
  @override
  void initState() {
    itemRef = FirebaseDatabase.instance.reference().child('data');
    item = Item(dewpoint: "dewPoint",kelembabantanah: "kelembabanTanah", nilaisensorhujan: 'nilaiSensorHujan',relativehumidity: 'relativeHumidity',suhu: 'suhu');
    itemRef.onChildAdded.listen(_onEntryAdded);
    itemRef.onChildChanged.listen(_onEntryChanged);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: height,
          child: AppBar(
            title: Text("Rose Monitoring"),
          )
        ),
        body: Column(
          children: <Widget>[
            /*Flexible(
              flex: 0,
              child: Center(
                child: Form(
                  key: formKey,
                  child: Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      ListTile(
                        leading: Icon(Icons.info),
                        title: TextFormField(
                          initialValue: "",
                          onSaved: (val) => item.dewpoint = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.info),
                        title: TextFormField(
                          initialValue: "",
                          onSaved: (val) => item.kelembabantanah= val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.info),
                        title: TextFormField(
                          initialValue: "",
                          onSaved: (val) => item.nilaisensorhujan = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.info),
                        title: TextFormField(
                          initialValue: "",
                          onSaved: (val) => item.relativehumidity = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.info),
                        title: TextFormField(
                          initialValue: "",
                          onSaved: (val) => item.suhu = val,
                          validator: (val) => val == "" ? val : null,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.send),
                        onPressed: () {
                          handleSubmit();
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),*/
            Container(
              height: MediaQuery.of(context).size.height/2 - 50.0,
              child: LineChartPage(),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
              child: Container(
                alignment: Alignment.centerLeft,
                child : Text("List Data Realtime",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height/2 - 88.0,
              alignment: Alignment.centerLeft,
              child: FirebaseAnimatedList(
                query: FirebaseDatabase.instance.reference().child('data'),
                itemBuilder: (BuildContext context, DataSnapshot snapshot,
                    Animation<double> animation, int index) {
                  return listData(
                      items[index].dewpoint,
                      items[index].kelembabantanah,
                      items[index].nilaisensorhujan,
                      items[index].relativehumidity,
                      items[index].suhu
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget listData(dewpoint,kelembaban,hujan,humidity,suhu) {
    var _hujan = int.parse('$hujan');
    assert(_hujan is int);
    print(_hujan);
    var statusHujan;
    if(_hujan > 500){
      statusHujan = 'Tidak Hujan';
    }else{
      statusHujan = 'Hujan';
    }
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 1,color: Colors.black),
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 10.0,
                offset: Offset(0,5.0),
              )],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Dewpoint : $dewpoint"),
              Text("Kelembabaan Tanah : $kelembaban"),
              Text("Status Hujan : $statusHujan"),
              Text("Relative Humidity : $humidity"),
              Text("Suhu : $suhu"),
            ],
          ),
        ),
      ),
    );
  }
}
class Item {
  String dewpoint;
  String kelembabantanah;
  String nilaisensorhujan;
  String relativehumidity;
  String suhu;
  String key;
  Item({this.dewpoint,this.kelembabantanah,this.nilaisensorhujan,this.relativehumidity,this.suhu});
  Item.fromSnapshot(DataSnapshot snapshot) : key = snapshot.key,
        dewpoint = snapshot.value['dewPoint'],
        kelembabantanah = snapshot.value['kelembabanTanah'],
        nilaisensorhujan = snapshot.value['nilaiSensorHujan'],
        relativehumidity = snapshot.value['relativeHumidity'],
        suhu = snapshot.value['suhu'];
  toJson(){
    return{
      'dewPoint' : dewpoint,
      'kelembabanTanah' : kelembabantanah,
      'nilaiSensorHujan' : nilaisensorhujan,
      'relativeHumidity' : relativehumidity,
      'suhu' : suhu

    };
  }
}

