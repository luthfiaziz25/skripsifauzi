import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:rose_monitoring/ui/v_home.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {

  Future _main() async {
        final FirebaseApp app = await FirebaseApp.configure(
          name: 'db2',
          options: const FirebaseOptions(
            googleAppID: '1:649968492817:android:ac626d046b7df9ee20ecb0',
            apiKey: 'AIzaSyAqnYoeESLmTE4bFFEVUTPb3i2hOQgjXOE',
            databaseURL: 'https://aqueous-scout-235701.firebaseio.com',
          ),
        );
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}
